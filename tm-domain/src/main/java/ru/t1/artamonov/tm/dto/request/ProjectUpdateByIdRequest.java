package ru.t1.artamonov.tm.dto.request;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public final class ProjectUpdateByIdRequest extends AbstractUserRequest {

    @Nullable
    private String projectId;

    @Nullable
    private String name;

    @Nullable
    private String description;

}
