package ru.t1.artamonov.tm.dto.response;

import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.model.Task;

public class TaskCreateResponse extends AbstractTaskResponse {

    public TaskCreateResponse(@Nullable final Task task) {
        super(task);
    }

}
