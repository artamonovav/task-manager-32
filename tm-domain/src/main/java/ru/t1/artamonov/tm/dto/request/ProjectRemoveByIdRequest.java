package ru.t1.artamonov.tm.dto.request;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public final class ProjectRemoveByIdRequest extends AbstractUserRequest {

    @Nullable
    private String projectId;

}
