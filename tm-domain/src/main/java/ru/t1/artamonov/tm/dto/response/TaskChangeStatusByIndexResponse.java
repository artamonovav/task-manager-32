package ru.t1.artamonov.tm.dto.response;

import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.model.Task;

public class TaskChangeStatusByIndexResponse extends AbstractTaskResponse {

    public TaskChangeStatusByIndexResponse(@Nullable final Task task) {
        super(task);
    }

}
