package ru.t1.artamonov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.client.TaskEndpointClient;
import ru.t1.artamonov.tm.command.AbstractCommand;
import ru.t1.artamonov.tm.enumerated.Role;
import ru.t1.artamonov.tm.enumerated.Status;
import ru.t1.artamonov.tm.model.Task;

import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    @NotNull
    protected TaskEndpointClient getTaskEndpointClient() {
        return serviceLocator.getTaskEndpointClient();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    protected void renderTasks(@NotNull final List<Task> tasks) {
        int index = 1;
        for (@NotNull final Task task : tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
    }

    protected void showTask(@NotNull final Task task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
    }

}
