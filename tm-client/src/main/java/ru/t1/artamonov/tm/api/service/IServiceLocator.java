package ru.t1.artamonov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.artamonov.tm.api.client.IEndpointClient;
import ru.t1.artamonov.tm.client.*;

public interface IServiceLocator {

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    ICommandService getCommandService();

    @NotNull
    IEndpointClient getConnectionEndpointClient();

    @NotNull
    AuthEndpointClient getAuthEndpointClient();

    @NotNull
    DomainEndpointClient getDomainEndpointClient();

    @NotNull
    ProjectEndpointClient getProjectEndpointClient();

    @NotNull
    TaskEndpointClient getTaskEndpointClient();

    @NotNull
    SystemEndpointClient getSystemEndpointClient();

    @NotNull
    UserEndpointClient getUserEndpointClient();

}
