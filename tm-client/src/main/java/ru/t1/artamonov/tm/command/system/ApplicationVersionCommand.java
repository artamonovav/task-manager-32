package ru.t1.artamonov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.dto.request.ServerAboutRequest;
import ru.t1.artamonov.tm.dto.request.ServerVersionRequest;
import ru.t1.artamonov.tm.dto.response.ServerAboutResponse;
import ru.t1.artamonov.tm.dto.response.ServerVersionResponse;

public final class ApplicationVersionCommand extends AbstractSystemCommand {

    @NotNull
    private static final String NAME = "version";

    @NotNull
    private static final String ARGUMENT = "-v";

    @NotNull
    private static final String DESCRIPTION = "Display program version.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.print("Client: ");
        System.out.println(getPropertyService().getApplicationVersion());
        @Nullable final ServerVersionRequest request = new ServerVersionRequest();
        @Nullable final ServerVersionResponse response =  serviceLocator.getSystemEndpointClient().getVersion(request);
        System.out.println("Server: " + response.getVersion());
    }

}
