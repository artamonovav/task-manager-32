package ru.t1.artamonov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.artamonov.tm.dto.request.ServerAboutRequest;
import ru.t1.artamonov.tm.dto.request.ServerVersionRequest;
import ru.t1.artamonov.tm.dto.response.AbstractResponse;

public interface ISystemEndpoint {

    @NotNull
    AbstractResponse getAbout(@NotNull ServerAboutRequest request);

    @NotNull
    AbstractResponse getVersion(@NotNull ServerVersionRequest request);

}
