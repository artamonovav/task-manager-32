package ru.t1.artamonov.tm.dto.response;

import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.model.Project;

public final class ProjectChangeStatusByIdResponse extends AbstractProjectResponse {

    public ProjectChangeStatusByIdResponse(@Nullable final Project project) {
        super(project);
    }

}
