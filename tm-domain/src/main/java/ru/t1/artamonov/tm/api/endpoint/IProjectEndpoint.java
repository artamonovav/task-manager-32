package ru.t1.artamonov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.artamonov.tm.dto.request.*;
import ru.t1.artamonov.tm.dto.response.*;

public interface IProjectEndpoint {

    @NotNull
    ProjectChangeStatusByIdResponse changeProjectStatusById(@NotNull ProjectChangeStatusByIdRequest request);

    @NotNull
    ProjectChangeStatusByIndexResponse changeProjectStatusByIndex(@NotNull ProjectChangeStatusByIndexRequest request);

    @NotNull
    ProjectClearResponse clearProject(@NotNull ProjectClearRequest request);

    @NotNull
    ProjectCreateResponse createProject(@NotNull ProjectCreateRequest request);

    @NotNull
    ProjectGetByIdResponse getProjectById(@NotNull ProjectGetByIdRequest request);

    @NotNull
    ProjectGetByIndexResponse getProjectByIndex(@NotNull ProjectGetByIndexRequest request);

    @NotNull
    ProjectListResponse listProject(@NotNull ProjectListRequest request);

    @NotNull
    ProjectRemoveByIdResponse removeById(@NotNull ProjectRemoveByIdRequest request);

    @NotNull
    ProjectRemoveByIndexResponse removeByIndex(@NotNull ProjectRemoveByIndexRequest request);

    @NotNull
    ProjectUpdateByIdResponse updateById(@NotNull ProjectUpdateByIdRequest request);

    @NotNull
    ProjectUpdateByIndexResponse updateByIndex(@NotNull ProjectUpdateByIndexRequest request);

}
