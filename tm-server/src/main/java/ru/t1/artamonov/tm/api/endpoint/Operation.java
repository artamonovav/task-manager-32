package ru.t1.artamonov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.artamonov.tm.dto.request.AbstractRequest;
import ru.t1.artamonov.tm.dto.response.AbstractResponse;

@FunctionalInterface
public interface Operation<RQ extends AbstractRequest, RS extends AbstractResponse> {

    @NotNull
    RS execute(@NotNull RQ request);

}
