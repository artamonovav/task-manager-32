package ru.t1.artamonov.tm.client;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.api.client.IEndpointClient;
import ru.t1.artamonov.tm.dto.response.ApplicationErrorResponse;

import java.io.*;
import java.net.Socket;

@Getter
@Setter
public abstract class AbstractEndpointClient implements IEndpointClient {

    @NotNull
    private String host = "localhost";

    @NotNull
    private Integer port = 6060;

    @NotNull
    private Socket socket;

    public AbstractEndpointClient() {
    }

    public AbstractEndpointClient(@NotNull final String host, @NotNull final Integer port) {
        this.host = host;
        this.port = port;
    }

    public AbstractEndpointClient(@NotNull final AbstractEndpointClient client) {
        this.host = client.host;
        this.port = client.port;
        this.socket = client.socket;
    }

//    @NotNull
//    @Override
//    public Object call(@NotNull final Object object) throws IOException, ClassNotFoundException {
//        getObjectOutputStream().writeObject(object);
//        return getObjectInputStream().readObject();
//    }

    @NotNull
    public <T> T call(@Nullable final Object data, @NotNull final Class<T> clazz) throws IOException, ClassNotFoundException {
        getObjectOutputStream().writeObject(data);
        @NotNull final Object result = getObjectInputStream().readObject();
        if (result instanceof ApplicationErrorResponse) {
            @NotNull final ApplicationErrorResponse response = (ApplicationErrorResponse) result;
            throw new RuntimeException(response.getMessage());
        }
        return (T) result;
    }

    private ObjectOutputStream getObjectOutputStream() throws IOException {
        return new ObjectOutputStream(getOutputStream());
    }

    private ObjectInputStream getObjectInputStream() throws IOException {
        return new ObjectInputStream(getInputStream());
    }

    @Nullable
    private OutputStream getOutputStream() throws IOException {
        if (socket == null) return null;
        return socket.getOutputStream();
    }

    @Nullable
    private InputStream getInputStream() throws IOException {
        if (socket == null) return null;
        return socket.getInputStream();
    }

    public Socket connect() throws IOException {
        socket = new Socket(host, port);
        return socket;
    }

    public Socket disconnect() throws IOException {
        if (socket == null) return null;
        socket.close();
        return socket;
    }

}
